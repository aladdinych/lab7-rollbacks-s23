## Setup

1. First set up python virtual env

2. Then, `docker-compose up` for postgresql:

```zsh

➜  lab7-rollbacks-s23 git:(main) ✗ docker-compose up

[+] Running 14/1
 ✔ db 13 layers [⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿]      0B/0B      Pulled                                                                                                          19.4s 
[+] Running 3/3
 ✔ Network lab7-rollbacks-s23_default  Created                                                                                                                   0.0s 
 ✔ Volume "lab7-rollbacks-s23_data"    Created                                                                                                                   0.0s 
 ✔ Container postgres                  Created                                                                                                                   0.3s 
Attaching to postgres
postgres  | The files belonging to this database system will be owned by user "postgres".
postgres  | This user must also own the server process.
postgres  | 
postgres  | The database cluster will be initialized with locale "en_US.utf8".
postgres  | The default database encoding has accordingly been set to "UTF8".
postgres  | The default text search configuration will be set to "english".
postgres  | 
postgres  | Data page checksums are disabled.
postgres  | 
postgres  | fixing permissions on existing directory /var/lib/postgresql/data ... ok
postgres  | creating subdirectories ... ok
postgres  | selecting dynamic shared memory implementation ... posix
postgres  | selecting default max_connections ... 100
postgres  | selecting default shared_buffers ... 128MB
postgres  | selecting default time zone ... Etc/UTC
postgres  | creating configuration files ... ok
postgres  | running bootstrap script ... ok
postgres  | performing post-bootstrap initialization ... ok
postgres  | syncing data to disk ... ok
postgres  | 
postgres  | 
postgres  | Success. You can now start the database server using:
postgres  | 
postgres  |     pg_ctl -D /var/lib/postgresql/data -l logfile start
postgres  | 
postgres  | initdb: warning: enabling "trust" authentication for local connections
postgres  | initdb: hint: You can change this by editing pg_hba.conf or using the option -A, or --auth-local and --auth-host, the next time you run initdb.
postgres  | waiting for server to start....2023-04-19 12:50:42.667 UTC [35] LOG:  starting PostgreSQL 15.2 (Debian 15.2-1.pgdg110+1) on aarch64-unknown-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit
postgres  | 2023-04-19 12:50:42.668 UTC [35] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
postgres  | 2023-04-19 12:50:42.676 UTC [38] LOG:  database system was shut down at 2023-04-19 12:50:42 UTC
postgres  | 2023-04-19 12:50:42.683 UTC [35] LOG:  database system is ready to accept connections
postgres  |  done
postgres  | server started
postgres  | 
postgres  | /usr/local/bin/docker-entrypoint.sh: ignoring /docker-entrypoint-initdb.d/*
postgres  | 
postgres  | waiting for server to shut down....2023-04-19 12:50:42.784 UTC [35] LOG:  received fast shutdown request
postgres  | 2023-04-19 12:50:42.785 UTC [35] LOG:  aborting any active transactions
postgres  | 2023-04-19 12:50:42.787 UTC [35] LOG:  background worker "logical replication launcher" (PID 41) exited with exit code 1
postgres  | 2023-04-19 12:50:42.789 UTC [36] LOG:  shutting down
postgres  | 2023-04-19 12:50:42.790 UTC [36] LOG:  checkpoint starting: shutdown immediate
postgres  | 2023-04-19 12:50:42.793 UTC [36] LOG:  checkpoint complete: wrote 3 buffers (0.0%); 0 WAL file(s) added, 0 removed, 0 recycled; write=0.002 s, sync=0.001 s, total=0.005 s; sync files=2, longest=0.001 s, average=0.001 s; distance=0 kB, estimate=0 kB
postgres  | 2023-04-19 12:50:42.799 UTC [35] LOG:  database system is shut down
postgres  |  done
postgres  | server stopped
postgres  | 
postgres  | PostgreSQL init process complete; ready for start up.
postgres  | 
postgres  | 2023-04-19 12:50:42.908 UTC [1] LOG:  starting PostgreSQL 15.2 (Debian 15.2-1.pgdg110+1) on aarch64-unknown-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit
postgres  | 2023-04-19 12:50:42.909 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
postgres  | 2023-04-19 12:50:42.909 UTC [1] LOG:  listening on IPv6 address "::", port 5432
postgres  | 2023-04-19 12:50:42.910 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
postgres  | 2023-04-19 12:50:42.914 UTC [49] LOG:  database system was shut down at 2023-04-19 12:50:42 UTC
postgres  | 2023-04-19 12:50:42.918 UTC [1] LOG:  database system is ready to accept connections
```

3. After that, open second terminal to connect to postgres: 

```zsh
➜  lab7-rollbacks-s23 git:(main) ✗ docker exec -it postgres bash
postgres@postgres:/$ 
```

4. Create DB, tables and stuff

```zsh
postgres=# \c lab
You are now connected to database "lab" as user "postgres".
lab=# CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE
lab=# CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
CREATE TABLE
```

5. Fill db with Sample data

```zsh
lab=# INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT 0 1
lab=# INSERT INTO PLayer (username, balance) VALUES ('Bob', 200);
INSERT 0 1
lab=# INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
INSERT 0 1
```


## Part 1

+ The two queries for buy_decrease_balance and buy_decrease_stock should be executed as a single database transaction to ensure atomicity. To achieve this, we can use 'with' statement and psycopg2 connection context manager. This will help ensure that the transaction is either committed or rolled back in case of an exception.

> Before running `buy_product("Alice", "marshmello", 1)`:

```zsh
lab=# SELECT * FROM player;
 username | balance 
----------+---------
 Alice    |     100
 Bob      |     200
(2 rows)

lab=# SELECT * FROM shop;
  product   | in_stock | price 
------------+----------+-------
 marshmello |       10 |    10
(1 row)
```

> After running `buy_product("Alice", "marshmello", 1)`:

`➜  lab7-rollbacks-s23 git:(main) ✗ python3 buy_product.py`

```zsh
lab=# SELECT * FROM player;
 username | balance 
----------+---------
 Bob      |     200
 Alice    |      99
(2 rows)

lab=# SELECT * FROM shop;
  product   | in_stock | price 
------------+----------+-------
 marshmello |        9 |    10
(1 row)
```

+ As expected, The "player" table shows that
Alice's balance has been reduced by
the price of the marshmello (10) and
is now 99. The "shop" table shows that
the in_stock value of marshmello has been
reduced by 1 and is now 9. This confirms that the buy_product function works as expected!


## Part 2

+ Create a new table for the player inventory.

```zsh
CREATE TABLE Inventory (
  username TEXT REFERENCES Player(username) NOT NULL,
  product TEXT REFERENCES Shop(product) NOT NULL,
  amount INT CHECK(amount >= 0),
  UNIQUE(username, product)
);
```

+ Add these queries to .py file:
```zsh
read_inventory = "SELECT amount FROM Inventory WHERE username = %s AND product = %s"
create_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%s, %s, %s)"
update_inventory = "UPDATE Inventory SET amount = amount + %s WHERE username = %s AND product = %s"
```

+ Include inventory checks and updates in the `buy_product()` function.
(function's called `buy_product_v2` in the python script)


## Part 3 (Extra)

> So, imagine if the function goes haywire after committing the transaction, and then the client decides to give it another shot. We might end up with duplicates, which we really don't want. To dodge this problem, we can whip up a new table that keeps tabs on successful transactions, and we'll use unique IDs for each of them. By checking and updating this table while the transaction's happening, we can steer clear of any annoying duplicates.

## Part 4 (Extra)

> Now, let's say we're dealing with a bunch of different systems all at once. If something goes wrong, we can create a sort of "undo" operation, also known as a compensating transaction, to cancel out the effects of the failed one. For example, if we take money out of a user's balance in the database but then the external system messes up and doesn't withdraw the real cash, we can make a compensating transaction that puts the money back where it belongs.

> Another thing we can do is set up some retries and checks with the external system, making sure everything's good before we go ahead and change the balance in the database.