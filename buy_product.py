import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
read_inventory = "SELECT amount FROM Inventory WHERE username = %s AND product = %s"
create_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%s, %s, %s)"
update_inventory = "UPDATE Inventory SET amount = amount + %s WHERE username = %s AND product = %s"


conn_str = "dbname=lab user=postgres password=postgres host=localhost port=5432"


def buy_product(username, product, amount):
    with psycopg2.connect(conn_str) as conn:
        with conn.cursor() as cur:
            buy_decrease_balance = "UPDATE Player SET balance = balance - %s WHERE username = %s"
            buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %s WHERE product = %s AND in_stock >= %s"
            
            try:
                cur.execute(buy_decrease_balance, (amount, username))
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
                cur.execute(buy_decrease_stock, (amount, product, amount))
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except Exception as e:
                raise e


# def buy_product(username, product, amount):
#     obj = {"product": product, "username": username, "amount": amount}
#     with get_connection() as conn:
#         with conn.cursor() as cur:
#             try:
#                 cur.execute(buy_decrease_balance, obj)
#                 if cur.rowcount != 1:
#                     raise Exception("Wrong username")
#             except psycopg2.errors.CheckViolation as e:
#                 raise Exception("Bad balance")

#     with get_connection() as conn:
#         with conn.cursor() as cur:
#             try:
#                 cur.execute(buy_decrease_stock, obj)

#                 if cur.rowcount != 1:
#                     raise Exception("Wrong product or out of stock")
#             except psycopg2.errors.CheckViolation as e:
#                 raise Exception("Product is out of stock")



# buy_product('Alice', 'marshmello', 1)


def buy_product_v2(username, product, amount):
    with psycopg2.connect(conn_str) as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, (amount, username))
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
                cur.execute(buy_decrease_stock, (amount, product, amount))
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
                
                cur.execute(read_inventory, (username, product))
                inventory_amount = cur.fetchone()
                if not inventory_amount:
                    cur.execute(create_inventory, (username, product, amount))
                else:
                     inventory_amount = inventory_amount[0]
                if inventory_amount + amount > 100:
                    raise Exception("Inventory is full")
                cur.execute(update_inventory, (amount, username, product))
            except Exception as e:
                raise e
